﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PokerGUI.Enums;

namespace PokerGUI.Classes
{
    public class Jugador
    {
        string name;
        Carta[] mano;
        int puntos = 0;


        public Jugador(string name)
        {
            this.Mano = new Carta[5];
            //Hola desde visual studio code
            this.Name = name;
        }

        public string Name { get => name; set => name = value; }
        public int Puntos { get => puntos; set => puntos = value; }
        internal Carta[] Mano { get => mano; set => mano = value; }
    }
}
