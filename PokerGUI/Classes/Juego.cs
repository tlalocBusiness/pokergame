﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PokerGUI.Enums;


namespace PokerGUI.Classes
{
    class Juego
    {
        Baraja deck;
        List<Jugador> jugadores;

        public Juego(Baraja deck, List<Jugador> jugadores)
        {
            this.deck = deck;
            this.jugadores = jugadores;
            repartir();

            //------------ USADO PARA PROBAR MANOS PREFABRICADAS----------------//
            //jugadores[0].Mano[0] = new Carta(Valor.ace, Palo.Espada);
            //jugadores[0].Mano[1] = new Carta(Valor.ace, Palo.Diamante);
            //jugadores[0].Mano[2] = new Carta(Valor.joto, Palo.Corazon);
            //jugadores[0].Mano[3] = new Carta(Valor.rey, Palo.Corazon);
            //jugadores[0].Mano[4] = new Carta(Valor.rey, Palo.Espada);

            //jugadores[1].Mano[0] = new Carta(Valor.dos, Palo.Trebol);
            //jugadores[1].Mano[1] = new Carta(Valor.dos, Palo.Corazon);
            //jugadores[1].Mano[2] = new Carta(Valor.dos, Palo.Diamante);
            //jugadores[1].Mano[3] = new Carta(Valor.cinco, Palo.Diamante);
            //jugadores[1].Mano[4] = new Carta(Valor.seis, Palo.Trebol);

        }

        private void repartir()
        {
            for (int i = 0; i < jugadores[0].Mano.Length; i++)
            {
                foreach (Jugador pl in jugadores)
                {
                    pl.Mano[i] = deck.CartaStack.Pop();
                }
            }
        }

        public void ordenar(Carta[] mano)
        {
            int pos_min;
            Carta temp;

            for (int i = 0; i < mano.Length - 1; i++)
            {
                pos_min = i;

                for (int j = i + 1; j < mano.Length; j++)
                {
                    if (mano[j].Valor.GetHashCode() < mano[pos_min].Valor.GetHashCode())
                    {
                        pos_min = j;
                    }
                }

                if (pos_min != i)
                {
                    temp = mano[i];
                    mano[i] = mano[pos_min];
                    mano[pos_min] = temp;
                }
            }
        }



        //-------------------JUGADAS DEL POKER

        public bool isFlush(Carta[] mano)
        {
            ordenar(mano);
            int flag = 0;
            for (int i = 0; i < mano.Length - 1; i++)
            {
                if (mano[i].Palo == mano[i + 1].Palo)
                {
                    flag++;
                }
            }

            return (flag == 4) ? true : false;
        }

        public bool isStraight(Carta[] mano)
        {
            ordenar(mano);
            int flag = 0;
            if (mano[0].Valor.GetHashCode() == 2 && mano[mano.Length - 1].ToInt() == 14)
            {
                if (mano[1].Valor.GetHashCode() == 3 && mano[2].ToInt() == 4)
                {
                    if (mano[3].ToInt() == 5)
                    {
                        return true;
                    }
                }

            }
            for (int i = 0; i < mano.Length - 1; i++)
            {
                if (mano[i].Valor.GetHashCode() + 1 == mano[i + 1].Valor.GetHashCode())
                {
                    flag++;
                }
            }

            return (flag == 4) ? true : false;
        }

        public bool isStraightFlush(Carta[] mano)
        {
            ordenar(mano);
            if (isStraight(mano) != false && isFlush(mano) != false)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool isRoyal(Carta[] mano)
        {
            ordenar(mano);
            int flag = 0;
            if (mano[0].Valor.GetHashCode() == 10)
            {
                for (int i = 0; i < mano.Length - 1; i++)
                {
                    if (mano[i].Valor.GetHashCode() + 1 == mano[i + 1].Valor.GetHashCode())
                    {
                        flag++;
                    }
                }
            }


            return (flag == 4 && isFlush(mano) != false) ? true : false;
        }

        public bool is4s(Carta[] mano)
        {
            ordenar(mano);
            bool a1, a2;

            if (mano.Length != 5)
            {
                return false;
            }

            a1 = mano[0].ToInt() == mano[1].ToInt() &&
                 mano[1].ToInt() == mano[2].ToInt() &&
                 mano[2].ToInt() == mano[3].ToInt();

            a2 = mano[1].ToInt() == mano[2].ToInt() &&
                 mano[2].ToInt() == mano[3].ToInt() &&
                 mano[3].ToInt() == mano[4].ToInt();

            return (a1 || a2);
        }

        public bool isFullHouse(Carta[] mano)
        {
            ordenar(mano);
            bool a1, a2;

            if (mano.Length != 5)
            {
                return false;
            }

            a1 = mano[0].ToInt() == mano[1].ToInt() &&
                 mano[1].ToInt() == mano[2].ToInt() &&
                 mano[3].ToInt() == mano[4].ToInt();

            a2 = mano[0].ToInt() == mano[1].ToInt() &&
                 mano[2].ToInt() == mano[3].ToInt() &&
                 mano[3].ToInt() == mano[4].ToInt();

            return (a1 || a2);
        }

        public bool is3s(Carta[] mano)
        {
            ordenar(mano);
            bool a1, a2, a3;

            if (mano.Length != 5)
            {
                return false;
            }

            a1 = mano[0].ToInt() == mano[1].ToInt() &&
                 mano[1].ToInt() == mano[2].ToInt();

            a2 = mano[1].ToInt() == mano[2].ToInt() &&
                 mano[2].ToInt() == mano[3].ToInt();

            a3 = mano[2].ToInt() == mano[3].ToInt() &&
                 mano[3].ToInt() == mano[4].ToInt();

            return (a1 || a2 || a3);
        }

        public bool is2Pairs(Carta[] mano)
        {
            ordenar(mano);
            bool a1, a2, a3;

            if (mano.Length != 5)
            {

                return false;
            }

            a1 = mano[0].ToInt() == mano[1].ToInt() &&
                 mano[2].ToInt() == mano[3].ToInt();

            a2 = mano[0].ToInt() == mano[1].ToInt() &&
                 mano[3].ToInt() == mano[4].ToInt();

            a3 = mano[1].ToInt() == mano[2].ToInt() &&
                 mano[3].ToInt() == mano[4].ToInt();

            return (a1 || a2 || a3);
        }

        public bool isPair(Carta[] mano)
        {
            ordenar(mano);
            bool a1, a2, a3, a4;

            if (mano.Length != 5)
            {
                return false;
            }
            a1 = mano[0].ToInt() == mano[1].ToInt();

            a2 = mano[1].ToInt() == mano[2].ToInt();

            a3 = mano[2].ToInt() == mano[3].ToInt();

            a4 = mano[3].ToInt() == mano[4].ToInt();

            return (a1 || a2 || a3 || a4);
        }

        //------------------DEFINE LA MANO QUE TIENE EL JUGADOR

        public string checar(Jugador jugador)
        {
            ordenar(jugador.Mano);
            if (isRoyal(jugador.Mano))
            {
                jugador.Puntos = 2000;
                return "Royal";
            }
            else if (isStraightFlush(jugador.Mano))
            {
                jugador.Puntos = 1800;
                jugador.Puntos += getStraight(jugador.Mano);
                return "StraightFlush";
            }
            else if (is4s(jugador.Mano))
            {
                jugador.Puntos = 1600;
                jugador.Puntos += get4s(jugador.Mano);
                return "4 of a kind of " + get4s(jugador.Mano);
            }
            else if (isFullHouse(jugador.Mano))
            {
                jugador.Puntos = 1400;
                jugador.Puntos += getFullHouse(jugador.Mano);
                return "FullHouse with '3 of a Kind' of " + getFullHouse(jugador.Mano);
            }
            else if (isFlush(jugador.Mano))
            {
                jugador.Puntos = 1200;
                jugador.Puntos += getMax(jugador.Mano);
                return "Flush of " + jugador.Mano[0].Palo;
            }
            else if (isStraight(jugador.Mano))
            {
                jugador.Puntos = 1000;
                jugador.Puntos += getStraight(jugador.Mano);
                return "Straight - starting at " + getStraight(jugador.Mano);
            }
            else if (is3s(jugador.Mano))
            {
                jugador.Puntos = 800;
                jugador.Puntos += get3s(jugador.Mano);
                return "3 of a kind of " + get3s(jugador.Mano);
            }
            else if (is2Pairs(jugador.Mano))
            {
                jugador.Puntos = 400;
                jugador.Puntos += get2Pairs(jugador.Mano);
                jugador.Puntos += is2PairsDiferent(jugador.Mano);
                return "2 Pairs - Highest pair of " + getBestPair(jugador.Mano);
            }
            else if (isPair(jugador.Mano))
            {
                jugador.Puntos = 200;
                jugador.Puntos += getPair(jugador.Mano);
                return "Pair of " + getPair(jugador.Mano).ToString();
            }
            else
            {
                jugador.Puntos = getMax(jugador.Mano);
                return "Carta Mayor " + jugador.Mano[4].ToInt().ToString();
            }
        }

        //------------------CAMBIA LAS CARTAS QUE EL JUGADOR NO QUIERE

        public void cambiarCartas(Carta[] mano, List<int> posiciones)
        {
            foreach (int pos in posiciones)
            {
                mano[pos] = deck.CartaStack.Pop();
            }
        }
        //---------------Metodos para tener la carta

        private int getMax(Carta[] mano)
        {
            return mano[4].ToInt();
        }

        private int getStraight(Carta[] mano)
        {
            return mano[0].ToInt();
        }

        private int get4s(Carta[] mano)
        {
            ordenar(mano);
            bool a1, a2;

            if (mano.Length != 5)
            {
                return -1;
            }

            a1 = mano[0].ToInt() == mano[1].ToInt() &&
                 mano[1].ToInt() == mano[2].ToInt() &&
                 mano[2].ToInt() == mano[3].ToInt();

            a2 = mano[1].ToInt() == mano[2].ToInt() &&
                 mano[2].ToInt() == mano[3].ToInt() &&
                 mano[3].ToInt() == mano[4].ToInt();

            if (a1)
            {
                return mano[0].ToInt();
            }
            else if (a2)
            {
                return mano[1].ToInt();
            }
            else
            {
                return -1;
            }
        }

        private int getFullHouse(Carta[] mano)
        {
            return mano[2].ToInt();
        }

        private int get3s(Carta[] mano)
        {
            ordenar(mano);
            bool a1, a2, a3;

            if (mano.Length != 5)
            {
                return -1;
            }

            a1 = mano[0].ToInt() == mano[1].ToInt() &&
                 mano[1].ToInt() == mano[2].ToInt();

            a2 = mano[1].ToInt() == mano[2].ToInt() &&
                 mano[2].ToInt() == mano[3].ToInt();

            a3 = mano[2].ToInt() == mano[3].ToInt() &&
                 mano[3].ToInt() == mano[4].ToInt();

            if (a1)
            {
                return mano[0].ToInt();
            }
            else if (a2)
            {
                return mano[1].ToInt();
            }
            else if (a3)
            {
                return mano[2].ToInt();
            }

            return -1;
        }

        private int get2Pairs(Carta[] mano)
        {
            if (mano[0].ToInt() == mano[1].ToInt() &&
                 mano[2].ToInt() == mano[3].ToInt())
            {
                return mano[0].ToInt()+(mano[2].ToInt()*14);
            }
            else if (mano[0].ToInt() == mano[1].ToInt() &&
                mano[3].ToInt() == mano[4].ToInt())
            {
                return mano[0].ToInt() + (mano[3].ToInt()*14);
            }
            else
            {
                return mano[1].ToInt() + (mano[3].ToInt()*14);
            }
        }

        private int getBestPair(Carta[] mano)
        {
            return mano[3].ToInt();
        }

        private int getPair(Carta[] mano)
        {
            ordenar(mano);
            int b1, b2, b3, b4;

            if (mano[0].ToInt() == mano[1].ToInt())
            {
                b1 = mano[0].ToInt();
                return b1;
            }

            if (mano[1].ToInt() == mano[2].ToInt())
            {
                b2 = mano[1].ToInt();
                return b2;
            }

            if (mano[2].ToInt() == mano[3].ToInt())
            {
                b3 = mano[2].ToInt();
                return b3;
            }


            if (mano[3].ToInt() == mano[4].ToInt())
            {
                b4 = mano[3].ToInt();
                return b4;
            }

            return -1;
        }

        private int is2PairsDiferent(Carta[] mano)
        {
            ordenar(mano);

            if (mano[0].ToInt() == mano[1].ToInt() &&
                 mano[2].ToInt() == mano[3].ToInt())
            {
                return mano[4].ToInt();
            }else if(mano[0].ToInt() == mano[1].ToInt() &&
                 mano[3].ToInt() == mano[4].ToInt())
            {
                return mano[2].ToInt();
            }else 
            {
                return mano[0].ToInt();
            }
        }

        //----------------------EVALUAR GANADOR-------

        public Jugador detGanador(List<Jugador> jugadores)
        {

            for(int i = 0; i<jugadores.Count; i++)
            {
                if(jugadores[i].Puntos > jugadores[i + 1].Puntos) { return jugadores[i]; }

            }

            return null;
        }
    }
}
