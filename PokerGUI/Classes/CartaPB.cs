﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PokerGUI.Classes;
using PokerGUI.Enums;

namespace PokerGUI.Classes
{
    class CartaPB
    {

        public static void PBCarta(PictureBox pbox, Carta carta)
        {
            Palo ej1 = carta.Palo;
            //Carta ej = new Carta(Valor.ace, Palo.Corazon);
            switch (carta.Valor)

            {
                case Valor.ace:
                    if (carta.Palo == Palo.Corazon)
                    {
                        pbox.Image = Properties.Resources.ace_of_hearts;
                    }
                    else if (carta.Palo == Palo.Diamante)
                    {
                        pbox.Image = Properties.Resources.ace_of_diamonds;
                    }
                    else if (carta.Palo == Palo.Espada)
                    {
                        pbox.Image = Properties.Resources.ace_of_spades;
                    }
                    else if (carta.Palo == Palo.Trebol)
                    {
                        pbox.Image = Properties.Resources.ace_of_clubs;
                    }
                    break;

                case Valor.dos:
                    if (carta.Palo == Palo.Corazon)
                    {
                        pbox.Image = Properties.Resources._2_of_hearts;
                    }
                    else if (carta.Palo == Palo.Diamante)
                    {
                        pbox.Image = Properties.Resources._2_of_diamonds;
                    }
                    else if (carta.Palo == Palo.Espada)
                    {
                        pbox.Image = Properties.Resources._2_of_spades;
                    }
                    else if (carta.Palo == Palo.Trebol)
                    {
                        pbox.Image = Properties.Resources._3_of_clubs;
                    }
                    break;
                case Valor.tres:
                    if (carta.Palo == Palo.Corazon)
                    {
                        pbox.Image = Properties.Resources._3_of_hearts;
                    }
                    else if (carta.Palo == Palo.Diamante)
                    {
                        pbox.Image = Properties.Resources._3_of_diamonds;
                    }
                    else if (carta.Palo == Palo.Espada)
                    {
                        pbox.Image = Properties.Resources._3_of_spades;
                    }
                    else if (carta.Palo == Palo.Trebol)
                    {
                        pbox.Image = Properties.Resources._3_of_clubs;
                    }
                    break;
                case Valor.cuatro:
                    if (carta.Palo == Palo.Corazon)
                    {
                        pbox.Image = Properties.Resources._4_of_hearts;
                    }
                    else if (carta.Palo == Palo.Diamante)
                    {
                        pbox.Image = Properties.Resources._4_of_diamonds;
                    }
                    else if (carta.Palo == Palo.Espada)
                    {
                        pbox.Image = Properties.Resources._4_of_spades;
                    }
                    else if (carta.Palo == Palo.Trebol)
                    {
                        pbox.Image = Properties.Resources._4_of_clubs;
                    }
                    break;
                case Valor.cinco:
                    if (carta.Palo == Palo.Corazon)
                    {
                        pbox.Image = Properties.Resources._5_of_hearts;
                    }
                    else if (carta.Palo == Palo.Diamante)
                    {
                        pbox.Image = Properties.Resources._5_of_diamonds;
                    }
                    else if (carta.Palo == Palo.Espada)
                    {
                        pbox.Image = Properties.Resources._5_of_spades;
                    }
                    else if (carta.Palo == Palo.Trebol)
                    {
                        pbox.Image = Properties.Resources._5_of_clubs;
                    }
                    break;
                case Valor.seis:
                    if (carta.Palo == Palo.Corazon)
                    {
                        pbox.Image = Properties.Resources._6_of_hearts;
                    }
                    else if (carta.Palo == Palo.Diamante)
                    {
                        pbox.Image = Properties.Resources._6_of_diamonds;
                    }
                    else if (carta.Palo == Palo.Espada)
                    {
                        pbox.Image = Properties.Resources._6_of_spades;
                    }
                    else if (carta.Palo == Palo.Trebol)
                    {
                        pbox.Image = Properties.Resources._6_of_clubs;
                    }
                    break;
                case Valor.siete:
                    if (carta.Palo == Palo.Corazon)
                    {
                        pbox.Image = Properties.Resources._7_of_hearts;
                    }
                    else if (carta.Palo == Palo.Diamante)
                    {
                        pbox.Image = Properties.Resources._7_of_diamonds;
                    }
                    else if (carta.Palo == Palo.Espada)
                    {
                        pbox.Image = Properties.Resources._7_of_spades;
                    }
                    else if (carta.Palo == Palo.Trebol)
                    {
                        pbox.Image = Properties.Resources._7_of_clubs;
                    }
                    break;
                case Valor.ocho:
                    if (carta.Palo == Palo.Corazon)
                    {
                        pbox.Image = Properties.Resources._8_of_hearts;
                    }
                    else if (carta.Palo == Palo.Diamante)
                    {
                        pbox.Image = Properties.Resources._8_of_diamonds;
                    }
                    else if (carta.Palo == Palo.Espada)
                    {
                        pbox.Image = Properties.Resources._8_of_spades;
                    }
                    else if (carta.Palo == Palo.Trebol)
                    {
                        pbox.Image = Properties.Resources._8_of_clubs;
                    }
                    break;
                case Valor.nueve:
                    if (carta.Palo == Palo.Corazon)
                    {
                        pbox.Image = Properties.Resources._9_of_hearts;
                    }
                    else if (carta.Palo == Palo.Diamante)
                    {
                        pbox.Image = Properties.Resources._9_of_diamonds;
                    }
                    else if (carta.Palo == Palo.Espada)
                    {
                        pbox.Image = Properties.Resources._9_of_spades;
                    }
                    else if (carta.Palo == Palo.Trebol)
                    {
                        pbox.Image = Properties.Resources._9_of_clubs;
                    }
                    break;
                case Valor.diez:
                    if (carta.Palo == Palo.Corazon)
                    {
                        pbox.Image = Properties.Resources._10_of_hearts;
                    }
                    else if (carta.Palo == Palo.Diamante)
                    {
                        pbox.Image = Properties.Resources._10_of_diamonds;
                    }
                    else if (carta.Palo == Palo.Espada)
                    {
                        pbox.Image = Properties.Resources._10_of_spades;
                    }
                    else if (carta.Palo == Palo.Trebol)
                    {
                        pbox.Image = Properties.Resources._10_of_clubs;
                    }
                    break;
                case Valor.joto:
                    if (carta.Palo == Palo.Corazon)
                    {
                        pbox.Image = Properties.Resources.jack_of_hearts;
                    }
                    else if (carta.Palo == Palo.Diamante)
                    {
                        pbox.Image = Properties.Resources.jack_of_diamonds;
                    }
                    else if (carta.Palo == Palo.Espada)
                    {
                        pbox.Image = Properties.Resources.jack_of_spades;
                    }
                    else if (carta.Palo == Palo.Trebol)
                    {
                        pbox.Image = Properties.Resources.jack_of_clubs;
                    }
                    break;
                case Valor.reina:
                    if (carta.Palo == Palo.Corazon)
                    {
                        pbox.Image = Properties.Resources.queen_of_hearts;
                    }
                    else if (carta.Palo == Palo.Diamante)
                    {
                        pbox.Image = Properties.Resources.queen_of_diamonds;
                    }
                    else if (carta.Palo == Palo.Espada)
                    {
                        pbox.Image = Properties.Resources.queen_of_spades;
                    }
                    else if (carta.Palo == Palo.Trebol)
                    {
                        pbox.Image = Properties.Resources.queen_of_clubs;
                    }
                    break;
                case Valor.rey:
                    if (carta.Palo == Palo.Corazon)
                    {
                        pbox.Image = Properties.Resources.king_of_hearts;
                    }
                    else if (carta.Palo == Palo.Diamante)
                    {
                        pbox.Image = Properties.Resources.king_of_diamonds;
                    }
                    else if (carta.Palo == Palo.Espada)
                    {
                        pbox.Image = Properties.Resources.king_of_spades;
                    }
                    else if (carta.Palo == Palo.Trebol)
                    {
                        pbox.Image = Properties.Resources.king_of_clubs;
                    }
                    break;
            }
        }

    }
}