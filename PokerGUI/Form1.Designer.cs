﻿namespace PokerGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.BttnPlayAgain = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.LblScore = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.LblPlayer = new System.Windows.Forms.Label();
            this.BttnReset = new System.Windows.Forms.Button();
            this.Card5 = new System.Windows.Forms.PictureBox();
            this.Card4 = new System.Windows.Forms.PictureBox();
            this.Card3 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.Card2 = new System.Windows.Forms.PictureBox();
            this.Card1 = new System.Windows.Forms.PictureBox();
            this.Bttn1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Card5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Card4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Card3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Card2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Card1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(826, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(19, 497);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(22, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(804, 21);
            this.panel3.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(22, 472);
            this.panel4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(804, 25);
            this.panel4.TabIndex = 3;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.BttnPlayAgain);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.LblScore);
            this.panel5.Controls.Add(this.button1);
            this.panel5.Controls.Add(this.LblPlayer);
            this.panel5.Controls.Add(this.BttnReset);
            this.panel5.Controls.Add(this.Card5);
            this.panel5.Controls.Add(this.Card4);
            this.panel5.Controls.Add(this.Card3);
            this.panel5.Controls.Add(this.pictureBox6);
            this.panel5.Controls.Add(this.pictureBox5);
            this.panel5.Controls.Add(this.pictureBox4);
            this.panel5.Controls.Add(this.pictureBox3);
            this.panel5.Controls.Add(this.pictureBox2);
            this.panel5.Controls.Add(this.Card2);
            this.panel5.Controls.Add(this.Card1);
            this.panel5.Controls.Add(this.Bttn1);
            this.panel5.Controls.Add(this.pictureBox1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(22, 21);
            this.panel5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(804, 451);
            this.panel5.TabIndex = 4;
            // 
            // BttnPlayAgain
            // 
            this.BttnPlayAgain.FlatAppearance.BorderSize = 0;
            this.BttnPlayAgain.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BttnPlayAgain.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BttnPlayAgain.ForeColor = System.Drawing.Color.White;
            this.BttnPlayAgain.Location = new System.Drawing.Point(-1, 402);
            this.BttnPlayAgain.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BttnPlayAgain.Name = "BttnPlayAgain";
            this.BttnPlayAgain.Size = new System.Drawing.Size(105, 55);
            this.BttnPlayAgain.TabIndex = 10;
            this.BttnPlayAgain.Text = "Play Again";
            this.BttnPlayAgain.UseVisualStyleBackColor = true;
            this.BttnPlayAgain.Visible = false;
            this.BttnPlayAgain.Click += new System.EventHandler(this.BttnPlayAgain_Click);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(668, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = "Puntos: ";
            // 
            // LblScore
            // 
            this.LblScore.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            this.LblScore.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblScore.ForeColor = System.Drawing.Color.White;
            this.LblScore.Location = new System.Drawing.Point(733, 0);
            this.LblScore.Name = "LblScore";
            this.LblScore.Size = new System.Drawing.Size(44, 20);
            this.LblScore.TabIndex = 9;
            this.LblScore.Text = "0";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            this.button1.Enabled = false;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(637, 408);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(140, 48);
            this.button1.TabIndex = 8;
            this.button1.Text = "Next Player";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // LblPlayer
            // 
            this.LblPlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPlayer.ForeColor = System.Drawing.Color.White;
            this.LblPlayer.Location = new System.Drawing.Point(-4, 0);
            this.LblPlayer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblPlayer.Name = "LblPlayer";
            this.LblPlayer.Size = new System.Drawing.Size(124, 40);
            this.LblPlayer.TabIndex = 7;
            this.LblPlayer.Text = "Player";
            this.LblPlayer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BttnReset
            // 
            this.BttnReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            this.BttnReset.FlatAppearance.BorderSize = 0;
            this.BttnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BttnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BttnReset.ForeColor = System.Drawing.Color.White;
            this.BttnReset.Location = new System.Drawing.Point(599, 294);
            this.BttnReset.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BttnReset.Name = "BttnReset";
            this.BttnReset.Size = new System.Drawing.Size(53, 34);
            this.BttnReset.TabIndex = 3;
            this.BttnReset.Text = "Reset";
            this.BttnReset.UseVisualStyleBackColor = false;
            this.BttnReset.Click += new System.EventHandler(this.button2_Click);
            // 
            // Card5
            // 
            this.Card5.BackColor = System.Drawing.Color.White;
            this.Card5.Image = ((System.Drawing.Image)(resources.GetObject("Card5.Image")));
            this.Card5.Location = new System.Drawing.Point(526, 294);
            this.Card5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Card5.Name = "Card5";
            this.Card5.Size = new System.Drawing.Size(62, 88);
            this.Card5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Card5.TabIndex = 2;
            this.Card5.TabStop = false;
            this.Card5.Click += new System.EventHandler(this.Card1_Click_1);
            // 
            // Card4
            // 
            this.Card4.BackColor = System.Drawing.Color.White;
            this.Card4.Image = ((System.Drawing.Image)(resources.GetObject("Card4.Image")));
            this.Card4.Location = new System.Drawing.Point(444, 294);
            this.Card4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Card4.Name = "Card4";
            this.Card4.Size = new System.Drawing.Size(62, 88);
            this.Card4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Card4.TabIndex = 2;
            this.Card4.TabStop = false;
            this.Card4.Click += new System.EventHandler(this.Card1_Click_1);
            // 
            // Card3
            // 
            this.Card3.BackColor = System.Drawing.Color.White;
            this.Card3.Image = ((System.Drawing.Image)(resources.GetObject("Card3.Image")));
            this.Card3.Location = new System.Drawing.Point(362, 294);
            this.Card3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Card3.Name = "Card3";
            this.Card3.Size = new System.Drawing.Size(62, 88);
            this.Card3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Card3.TabIndex = 2;
            this.Card3.TabStop = false;
            this.Card3.Click += new System.EventHandler(this.Card1_Click_1);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(500, 72);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(44, 63);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 2;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Click += new System.EventHandler(this.Card1_Click_1);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(433, 72);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(44, 63);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 2;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.Card1_Click_1);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(371, 72);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(44, 63);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 2;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.Card1_Click_1);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(311, 72);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(44, 63);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.Card1_Click_1);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(247, 72);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(44, 63);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.Card1_Click_1);
            // 
            // Card2
            // 
            this.Card2.BackColor = System.Drawing.Color.White;
            this.Card2.Image = ((System.Drawing.Image)(resources.GetObject("Card2.Image")));
            this.Card2.Location = new System.Drawing.Point(280, 294);
            this.Card2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Card2.Name = "Card2";
            this.Card2.Size = new System.Drawing.Size(62, 88);
            this.Card2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Card2.TabIndex = 2;
            this.Card2.TabStop = false;
            this.Card2.Click += new System.EventHandler(this.Card1_Click_1);
            // 
            // Card1
            // 
            this.Card1.BackColor = System.Drawing.Color.White;
            this.Card1.Image = ((System.Drawing.Image)(resources.GetObject("Card1.Image")));
            this.Card1.Location = new System.Drawing.Point(200, 294);
            this.Card1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Card1.Name = "Card1";
            this.Card1.Size = new System.Drawing.Size(62, 88);
            this.Card1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Card1.TabIndex = 2;
            this.Card1.TabStop = false;
            this.Card1.Click += new System.EventHandler(this.Card1_Click_1);
            // 
            // Bttn1
            // 
            this.Bttn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            this.Bttn1.FlatAppearance.BorderSize = 0;
            this.Bttn1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Bttn1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Bttn1.ForeColor = System.Drawing.Color.White;
            this.Bttn1.Location = new System.Drawing.Point(281, 180);
            this.Bttn1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Bttn1.Name = "Bttn1";
            this.Bttn1.Size = new System.Drawing.Size(226, 57);
            this.Bttn1.TabIndex = 1;
            this.Bttn1.Text = "Hold";
            this.Bttn1.UseVisualStyleBackColor = false;
            this.Bttn1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(804, 451);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Margin = new System.Windows.Forms.Padding(2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(22, 116);
            this.panel6.TabIndex = 5;
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(0, 413);
            this.panel7.Margin = new System.Windows.Forms.Padding(2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(22, 84);
            this.panel7.TabIndex = 7;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(22, 497);
            this.panel1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            this.ClientSize = new System.Drawing.Size(845, 497);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Card5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Card4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Card3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Card2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Card1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox Card5;
        private System.Windows.Forms.PictureBox Card4;
        private System.Windows.Forms.PictureBox Card3;
        private System.Windows.Forms.PictureBox Card2;
        private System.Windows.Forms.PictureBox Card1;
        private System.Windows.Forms.Button Bttn1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button BttnReset;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label LblPlayer;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label LblScore;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button BttnPlayAgain;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel1;
    }
}

