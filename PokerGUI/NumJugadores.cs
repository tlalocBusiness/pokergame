﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PokerGUI.Classes;

namespace PokerGUI
{
    public partial class NumJugadores : Form
    {
        List<Jugador> jugadores;
        public NumJugadores(List<Jugador> jugadores)
        {
            InitializeComponent();
            this.jugadores = jugadores;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Location = new Point(Cursor.Position.X - 400, Cursor.Position.Y);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(comboBox1.Text != "")
            {
                for (int i = 0; i < getNumber(comboBox1); i++)
                {
                    jugadores.Add(new Jugador("Player " + (i + 1).ToString()));
                }
                this.Close();
            }
        }

        private int getNumber(ComboBox list)
        {
            if (list.Text == "2 Players")
            {
                return 2;
            }
            else if (list.Text == "3 Players")
            {
                return 3;
            }
            else if (list.Text == "4 Players")
            {
                return 4;
            }
            else return 0;
        }
    }
}
