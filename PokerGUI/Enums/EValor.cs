﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerGUI.Enums
{
    public enum Valor
    {
        dos = 2,
        tres,
        cuatro,
        cinco,
        seis,
        siete,
        ocho,
        nueve,
        diez,
        joto,
        reina,
        rey,
        ace
    }
    class EValor
    {
        public static Valor getValor(int num)
        {
            switch (num)
            {
                case 2:
                    return Valor.dos;
                case 3:
                    return Valor.tres;
                case 4:
                    return Valor.cuatro;
                case 5:
                    return Valor.cinco;
                case 6:
                    return Valor.seis;
                case 7:
                    return Valor.siete;
                case 8:
                    return Valor.ocho;
                case 9:
                    return Valor.nueve;
                case 10:
                    return Valor.diez;
                case 11:
                    return Valor.joto;
                case 12:
                    return Valor.reina;
                case 13:
                    return Valor.rey;
                case 14:
                    return Valor.ace;
                default:
                    return 0;
            }
        }
    }
}
