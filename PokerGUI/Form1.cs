﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PokerGUI.Classes;

namespace PokerGUI
{
    public partial class Form1 : Form
    {

        List<PictureBox> pbList = new List<PictureBox>();
        Baraja deck = new Baraja();
        List<Jugador> jugadores = new List<Jugador>();
        NumJugadores numeroJugadores;
        Juego game;
        Jugador actual;
        Jugador ganador;
        List<Jugador> nuevaLista;


        public Form1()
        {
            InitializeComponent();
            init();
            this.numeroJugadores = new NumJugadores(jugadores);
            numeroJugadores.ShowDialog();
            this.game = new Juego(deck, jugadores);
            actual = jugadores[0];
            ponerCarta(actual.Mano, pbList);
            LblPlayer.Text = actual.Name;
        }

        private void init()
        {
            pbList.Add(Card1);
            pbList.Add(Card2);
            pbList.Add(Card3);
            pbList.Add(Card4);
            pbList.Add(Card5);
        }

        private List<Jugador> nuevaList()
        {
            nuevaLista = jugadores.OrderBy(item => item.Puntos).ToList();
            nuevaLista.Reverse();
            return nuevaLista;
            
        }
        //------------------Mover Cartas-----------------//
        private void Card1_Click_1(object sender, EventArgs e)
        {
            if (Bttn1.Enabled)
            {
                if (sender == Card1 && Card1.Top == 294)
                    Card1.Top = Card1.Top - 15;
                else if (sender == Card2 && Card2.Top == 294)
                    Card2.Top = Card2.Top - 15;
                else if (sender == Card3 && Card3.Top == 294)
                    Card3.Top = Card3.Top - 15;
                else if (sender == Card4 && Card4.Top == 294)
                    Card4.Top = Card4.Top - 15;
                else if (sender == Card5 && Card5.Top == 294)
                    Card5.Top = Card5.Top - 15;
                else if (sender == Card1 && Card1.Top == 279)
                    Card1.Top = Card1.Top + 15;
                else if (sender == Card2 && Card2.Top == 279)
                    Card2.Top = Card2.Top + 15;
                else if (sender == Card3 && Card3.Top == 279)
                    Card3.Top = Card3.Top + 15;
                else if (sender == Card4 && Card4.Top == 279)
                    Card4.Top = Card4.Top + 15;
                else if (sender == Card5 && Card5.Top == 279)
                    Card5.Top = Card5.Top + 15;
            }
            bool flag = false;
            for(int i=0; i < pbList.Count; i++)
            {
                if (pbList[i].Top == 279)
                {
                    flag = true;
                }
            }
            if (flag)
            {
                Bttn1.Text = "Draw";
            }
            else
            {
                Bttn1.Text = "Hold";
            }
        }

        //----------------Regresar Cartas---------------------//
        private void button2_Click(object sender, EventArgs e)
        {
            Bttn1.Text = "Hold";
            foreach (PictureBox card in pbList)
            {
                if (card.Top == 279)
                    card.Top = card.Top + 15;
            }
        }

        //--------------Regresa la posicion de las Cartas levantas----------------//
        private List<int> GetCardNum()
        {
            List<int> levantas = new List<int>();

            for(int i = 0; i < pbList.Count ; i++)
            {
                if (pbList[i].Top == 279)
                {
                    levantas.Add(i);
                }
            }

            return levantas;
        }

        //--------------Boton de hold/Draw----------------//
        int cont = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            if(cont < 2)
            {
                List<int> lista = new List<int>();
                lista = GetCardNum();

                game.cambiarCartas(actual.Mano, lista);
                ponerCarta(actual.Mano, pbList);
                Carta[] temp = new Carta[5];
                actual.Mano.CopyTo(temp, 0);
                Bttn1.Text = "Hold";
                foreach (PictureBox card in pbList)
                {
                    if (card.Top == 279)
                        card.Top = card.Top + 15;
                }
                Bttn1.Enabled = false;
                button1.Enabled = true;
                game.checar(actual); //Checa la mano del jugador y actualiza sus puntos
                
                LblScore.Text = actual.Puntos.ToString();
                actual.Mano = temp;
            }
        }
        //--------------------- Display Cards -------------------------//
        private void ponerCarta(Carta[] mano, List<PictureBox> lista)
        {
            for(int i = 0; i< lista.Count; i++)
            {
                CartaPB.PBCarta(lista[i], mano[i]);
            }
        }
        
        //-----------------------Siguiente Jugador-----------------//
        int now = 0;

        private void nextPlayer()
        {
            Bttn1.Enabled = true;
            button1.Enabled = false;
            //--------------Poner Cartas del Jugador-------//
            if(now != jugadores.Count - 1)
            {
                actual = jugadores[now + 1];
                ponerCarta(actual.Mano, pbList);
                LblPlayer.Text = actual.Name;
                BttnReset.PerformClick();
                LblScore.Text = actual.Puntos.ToString();
                ++now;
            }
            else
            {

                ++cont;
                now = 0;
                actual = jugadores[0];
                ponerCarta(actual.Mano, pbList);
                LblPlayer.Text = actual.Name;
                BttnReset.PerformClick();
                LblScore.Text = actual.Puntos.ToString();
            }
            if(cont > 1)
            {
                //------------Mostrar Ganador-------//
                ganador = game.detGanador(nuevaList());

                StringBuilder str = new StringBuilder();
                str.AppendLine("-------> "  + ganador.Name.ToUpper() + " WINS!! <-------");
                str.AppendLine();
                foreach (Jugador pl in jugadores)
                {
                    str.AppendLine("-> "+pl.Name + "-> " + pl.Puntos + " points");
                    str.AppendLine(game.checar(pl));
                    str.AppendLine();
                }
                
                MessageBox.Show(""+ str);
                ponerCarta(ganador.Mano, pbList);
                LblPlayer.Text = ganador.Name;
                BttnPlayAgain.Visible = true;
            }
            else
            {
                AutoClosingMessageBox.Show(actual.Name + "'s turn", " ", 1000);
            }
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            nextPlayer();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            BttnPlayAgain.Visible = false;

        }

        private void BttnPlayAgain_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
    }
}
