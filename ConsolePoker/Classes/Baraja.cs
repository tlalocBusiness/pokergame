﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsolePoker.Enums;

namespace ConsolePoker.Classes
{
    class Baraja
    {
        Stack<Carta> cartaStack;
        Carta[] cartas;
        static Random _random = new Random();

        internal Carta[] Cartas { get => cartas; set => cartas = value; }
        internal Stack<Carta> CartaStack { get => cartaStack; set => cartaStack = value; }

        public Baraja()
        {
            this.Cartas = new Carta[52];
            this.CartaStack = new Stack<Carta>();
            init();
            
        }

        static void Shuffle<T>(T[] array)
        {
            int n = array.Length;
            for (int i = 0; i < n; i++)
            {
                int r = i + _random.Next(n - i);
                T t = array[r];
                array[r] = array[i];
                array[i] = t;
            }
        }

        private void init()
        {
            for (int i = 2; i < 15; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    CartaStack.Push(new Carta(EValor.getValor(i), EPalo.getPalo(j)));
                }
            }

            for (int i = 0; i < 52; i++)
            {
                cartas[i] = CartaStack.Pop();
            }
            Shuffle<Carta>(Cartas);

            for (int i = 0; i < 52; i++)
            {
                CartaStack.Push(cartas[i]);
            }
            cartas = null;
        }

    }
}
