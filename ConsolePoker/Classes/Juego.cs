﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ConsolePoker.Classes
{
    class Juego
    {
        Baraja deck;
        List<Jugador> jugadores;

        public Juego(Baraja deck, List<Jugador> jugadores)
        {
            this.deck = deck;
            this.jugadores = jugadores;
            repartir();

        }

        private void repartir()
        {
            for (int i = 0; i < jugadores[0].Mano.Length; i++)
            {
                foreach (Jugador pl in jugadores)
                {
                    pl.Mano[i] = deck.CartaStack.Pop();
                }
            }
        }

        public void ordenar(Carta[] mano)
        {
            int pos_min;
            Carta temp;

            for (int i = 0; i < mano.Length - 1; i++)
            {
                pos_min = i;

                for (int j = i + 1; j < mano.Length; j++)
                {
                    if (mano[j].Valor.GetHashCode() < mano[pos_min].Valor.GetHashCode())
                    {
                        pos_min = j;
                    }
                }

                if (pos_min != i)
                {
                    temp = mano[i];
                    mano[i] = mano[pos_min];
                    mano[pos_min] = temp;
                }
            }
        }



        //-------------------JUGADAS DEL POKER

        public bool isFlush(Carta[] mano)
        {
            ordenar(mano);
            int flag = 0;
            for (int i = 0; i < mano.Length - 1; i++)
            {
                if (mano[i].Palo == mano[i + 1].Palo)
                {
                    flag++;
                }
            }

            return (flag == 4) ? true : false;
        }

        public bool isStraight(Carta[] mano)
        {
            ordenar(mano);
            int flag = 0;
            if(mano[0].Valor.GetHashCode()==2 && mano[mano.Length-1].ToInt() == 14)
            {
                if (mano[1].Valor.GetHashCode() == 3 && mano[2].ToInt() == 4)
                {
                    if(mano[3].ToInt() == 5)
                    {
                        return true;
                    }
                }
                    
            }
            for (int i = 0; i < mano.Length - 1; i++)
            {
                if (mano[i].Valor.GetHashCode() + 1 == mano[i + 1].Valor.GetHashCode())
                {
                    flag++;
                }
            }

            return (flag == 4) ? true : false;
        }

        public bool isStraightFlush(Carta[] mano)
        {
            ordenar(mano);
            if (isStraight(mano) != false && isFlush(mano) != false)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool isRoyal(Carta[] mano)
        {
            ordenar(mano);
            int flag = 0;
            if (mano[0].Valor.GetHashCode() == 10)
            {
                for (int i = 0; i < mano.Length - 1; i++)
                {
                    if (mano[i].Valor.GetHashCode() + 1 == mano[i + 1].Valor.GetHashCode())
                    {
                        flag++;
                    }
                }
            }


            return (flag == 4 && isFlush(mano) != false) ? true : false;
        }

        public bool is4s(Carta[] mano)
        {
            ordenar(mano);
            bool a1, a2;

            if (mano.Length != 5)
            {
                return false;
            }

            a1 = mano[0].ToInt() == mano[1].ToInt() &&
                 mano[1].ToInt() == mano[2].ToInt() &&
                 mano[2].ToInt() == mano[3].ToInt();

            a2 = mano[1].ToInt() == mano[2].ToInt() &&
                 mano[2].ToInt() == mano[3].ToInt() &&
                 mano[3].ToInt() == mano[4].ToInt();

            return(a1 || a2);
        }

        public bool isFullHouse(Carta[] mano)
        {
            ordenar(mano);
            bool a1, a2;

            if (mano.Length != 5)
            {
                return false;
            }
            
            a1 = mano[0].ToInt() == mano[1].ToInt() &&
                 mano[1].ToInt() == mano[2].ToInt() &&
                 mano[3].ToInt() == mano[4].ToInt();

            a2 = mano[0].ToInt() == mano[1].ToInt() &&
                 mano[2].ToInt() == mano[3].ToInt() &&
                 mano[3].ToInt() == mano[4].ToInt();

            return (a1 || a2);
        }

        public bool is3s(Carta[] mano)
        {
            ordenar(mano);
            bool a1, a2, a3;

            if (mano.Length != 5)
            {
                return false;
            }
            
            a1 = mano[0].ToInt() == mano[1].ToInt() &&
                 mano[1].ToInt() == mano[2].ToInt();
            
            a2 = mano[1].ToInt() == mano[2].ToInt() &&
                 mano[2].ToInt() == mano[3].ToInt();

            a3 = mano[2].ToInt() == mano[3].ToInt() &&
                 mano[3].ToInt() == mano[4].ToInt();

            return (a1 || a2 || a3);
        }

        public bool is2Pairs(Carta[] mano)
        {
            ordenar(mano);
            bool a1, a2, a3;

            if (mano.Length != 5)
            {

                return false;
            }
            
            a1 = mano[0].ToInt() == mano[1].ToInt() &&
                 mano[2].ToInt() == mano[3].ToInt();
            
            a2 = mano[0].ToInt() == mano[1].ToInt() &&
                 mano[3].ToInt() == mano[4].ToInt();

            a3 = mano[1].ToInt() == mano[2].ToInt() &&
                 mano[3].ToInt() == mano[4].ToInt();

            return (a1 || a2 || a3);
        }

        public bool isPair(Carta[] mano)
        {
            ordenar(mano);
            bool a1, a2, a3, a4;

            if (mano.Length != 5)
            {
                return false;
            }
            a1 = mano[0].ToInt() == mano[1].ToInt();
            
            a2 = mano[1].ToInt() == mano[2].ToInt();

            a3 = mano[2].ToInt() == mano[3].ToInt();

            a4 = mano[3].ToInt() == mano[4].ToInt();

            return (a1 || a2 || a3 || a4);
        }

        //------------------DEFINE LA MANO QUE TIENE EL JUGADOR

        public string checar(Carta[] mano)
        {
            if (isRoyal(mano))
            {
                return "Royal";
            }else if (isStraightFlush(mano))
            {
                return "StraightFlush";
            }else if (is4s(mano))
            {
                return "4 of a kind";
            }else if (isFullHouse(mano))
            {
                return "FullHouse";
            }else if (isFlush(mano))
            {
                return "Flush";
            }else if (isStraight(mano))
            {
                return "Straight";
            }else if (is3s(mano))
            {
                return "3 of a kind";
            }else if (is2Pairs(mano))
            {
                return "2 Pairs";
            }else if (isPair(mano))
            {
                return "Pair";
            }
            else
            {
                return "Carta Mayor " + mano[4].ToInt().ToString();
            }
        }

        //------------------CAMBIA LAS CARTAS QUE EL JUGADOR NO QUIERE

        public void cambiarCartas(Carta[] mano, List<int> posiciones)
        {
            foreach (int pos in posiciones)
            {
                mano[pos] = deck.CartaStack.Pop();
            }
        }

        //-----------------VALOR DE CADA MANO
        

        public const int STRAIGHT_FLUSH = 8;
        // + valueHighCard()
        public const int FOUR_OF_A_KIND = 7;
        // + Quads Card Rank
        public const int FULL_HOUSE = 6;
        // + SET card rank
        public const int FLUSH = 5;
        // + valueHighCard()
        public const int STRAIGHT = 4;
        // + valueHighCard()
        public const int SET = 3;
        // + Set card const
        public const int TWO_PAIRS = 2;
        // + High2*14^4+ Low2*14^2 + card
        public const int ONE_PAIR = 1;
        // + high*14^2 + high2*14^1 + low


        //---------------------EVALUAR LA MANO DE UN JUGADOR
        public int valueHighCard(Carta[] mano)
        {
            int val;
            //Se usa 14 porque es el valor de la carta mas alta
            ordenar(mano);
            
            val = mano[0].ToInt() + (14 * mano[1].ToInt()) + ((Convert.ToInt32(Math.Pow(14, 2))) * mano[2].ToInt()) + ((Convert.ToInt32(Math.Pow(14, 3))) * mano[3].ToInt()) + ((Convert.ToInt32(Math.Pow(14, 4))) * mano[4].ToInt());

            return val;
        }



        public
    }
}
