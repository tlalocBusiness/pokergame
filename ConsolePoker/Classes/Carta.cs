﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsolePoker.Enums;

namespace ConsolePoker.Classes
{
    class Carta
    {
        Valor valor;
        Palo palo;

        public Carta(Valor valor, Palo palo)
        {
            this.Valor = valor;
            this.Palo = palo;
        }

        public Valor Valor { get => valor; set => valor = value; }
        public Palo Palo { get => palo; set => palo = value; }

        public int ToInt()
        {
            return this.Valor.GetHashCode();
        }
    }
}
