﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsolePoker.Enums;

namespace ConsolePoker.Classes
{
    class Jugador
    {
        string name;
        Carta[] mano;


        public Jugador(string name)
        {
            this.Mano = new Carta[5];
            this.Name = name;
        }

        public string Name { get => name; set => name = value; }
        internal Carta[] Mano { get => mano; set => mano = value; }
    }
}
