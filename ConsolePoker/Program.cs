﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsolePoker.Classes;

namespace ConsolePoker
{
    class Program
    {
        static void Main(string[] args)
        {
            Baraja deck = new Baraja();
            List<Jugador> jugadores = new List<Jugador>();
            jugadores.Add(new Jugador("Salva"));
            jugadores.Add(new Jugador("Klaus"));


            Juego game = new Juego(deck, jugadores);

            foreach(Jugador pl in jugadores)
            {
                foreach(Carta carta in pl.Mano)
                {
                    Console.WriteLine(carta.Valor.GetHashCode() + " de " + carta.Palo);
                }
                Console.WriteLine("---------------------------------");
            }

            List<int> posiciones = new List<int>();

            Console.WriteLine("Numero de cartas que quieres cambiar: ");
            int j = int.Parse(Console.ReadLine());
            for(int i = 0; i< j; i++)
            {
                Console.WriteLine("Carta " + (i + 1).ToString());
                int pos = int.Parse(Console.ReadLine());
                posiciones.Add(pos-1);
            }

            game.cambiarCartas(jugadores[0].Mano, posiciones);
            Console.WriteLine("---------------------------------");

            foreach (Jugador pl in jugadores)
            {
                foreach (Carta carta in pl.Mano)
                {
                    Console.WriteLine(carta.Valor.GetHashCode() + " de " + carta.Palo);
                }
                Console.WriteLine("---------------------------------");
            }
        }
    }
}
